extends Control

var demo_events : Array[No_Twitch.EventSub_Subscription] = [
	
	No_Twitch.EventSub_Subscription.new("channel.update", {"broadcaster_user_id" : ""}, "", "2")
	
]
@onready var twitch : No_Twitch = %Twitch_Connection

func _ready():
	
	twitch.token_received.connect(save_token)
	%Get_User.pressed.connect(func():
		
		var resp = %Twitch_Connection.request_user_info()
		resp.response_received.connect(print_http_result)
		
		)
	
	%Get_Channel.pressed.connect(func():
		
		var resp = %Twitch_Connection.request_channel_info()
		resp.response_received.connect(print_http_result)
		
		)
	
	%Connect_Channel_Points.pressed.connect(func():
		
		await twitch.setup_eventsub_connection()
		print("Passed Await")
		twitch.eventsub_socket.notif_received.connect(eventsub_notif_received)
		
		var resp = await twitch.subscribe_to_channel_points(twitch.user_info.id)
		resp.response_received.connect(print_http_result)
		
		)
	%Get_Followers.pressed.connect(func():
		
		var resp = twitch.twitch_request("https://api.twitch.tv/helix/channels/followers?broadcaster_id=" + twitch.user_info.id)
		await resp.response_received
		print(resp.inf_data)
		
		)
	
	load_token()
	

func save_token(token):
	
	var res = TokenSaver.new()
	res.token = token
	ResourceSaver.save(res, "user://token.tres")
	

func load_token():
	
	if !FileAccess.file_exists("user://token.tres"):
		
		return
		
	
	var res = ResourceLoader.load("user://token.tres")
	$Twitch_Connection.token = res.token
	%Twitch_Connection.cache_user_data()
	

func print_http_result(data):
	
	print(data)
	

func eventsub_notif_received(info):
	print(info)
	printt(info.payload.event.reward.id, info.payload.event.user_input, info.payload.event.reward.title)
	
