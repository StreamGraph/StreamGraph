extends HBoxContainer

var channel : String

func _ready():
	
	%Channel_Input.text_changed.connect(update_channel)
	%Join_Chat.pressed.connect(join_chat)
	%Start_Chat_Connection.pressed.connect(start_chat_connection)
	
	%Chat_Msg.text_submitted.connect(send_chat)
	%Send_Button.pressed.connect(func(): send_chat(%Chat_Msg.text))
	

func update_channel(new_channel):
	
	channel = new_channel
	

func start_chat_connection():
	
	%Twitch_Connection.setup_chat_connection(channel)
	

func join_chat():
	
	%Twitch_Connection.join_channel(channel)
	

func send_chat(chat : String):
	
	%Twitch_Connection.send_chat(chat, %Channel.text)
	
