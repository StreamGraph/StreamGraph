extends Button


@onready var twitch_connection : No_Twitch = $"../../Twitch_Connection"

func _pressed():
	
	OS.shell_open(twitch_connection.authenticate_with_twitch())
	
