# StreamGraph is a node graph-based virtual stream deck and livestream automation tool.
## ⚠️ StreamGraph is at an EARLY ALPHA stage! Some things will break! ⚠️
StreamGraph lets you automate your livestream with a powerful and easy to understand node graph workflow.

![StreamGraph screenshot](img/example1.png)

It can connect to Twitch and OBS, allowing you to take stream interactivity to the next level.

![StreamGraph screenshot](img/example2.png)

## Current state and capabilities
**StreamGraph is at a very early stage of development**. We cannot guarantee stability at this point and things may change dramatically between releases. As development progresses, the API and nodes will stabilize. If you encounter any issues, please report them on [the issue tracker](https://codeberg.org/Eroax/StreamGraph/issues) with as much detail about how to reproduce them as possible.

The app can currently connect to Twitch and read and send chats in a single channel.
It can also connect to a single OBS instance (utilizing OBS-WebSocket, which is bundled with modern versions of OBS).

For more information about nodes that currently exist, [check out the wiki.](https://codeberg.org/Eroax/StreamGraph/wiki)

You can also join Eroax's discord, which has a dedicated Streamgraph channel if you just want to talk or ask questions: [Discord](https://discord.gg/kQmKKeM2Nu)

## License

Licensed under GPLv3. See file [COPYING](https://codeberg.org/StreamGraph/StreamGraph/src/branch/main/COPYING) for full license text.