# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
class_name Connections

static var obs_websocket: NoOBSWS
static var twitch: No_Twitch

static func _twitch_eventsub_event_received(event_data : Dictionary):
	
	DeckHolder.send_event(&"twitch_eventsub", event_data)
	

static func _twitch_chat_received(msg_dict : Dictionary):
	
	DeckHolder.send_event(&"twitch_chat", msg_dict)


static func _obs_event_received(event_data: Dictionary):
	DeckHolder.send_event(&"obs_event", event_data)


## Temporary Function for loading generic Connection credentials from user:// as a [CredSaver] Resource
static func load_credentials(service : String):
	
	if not FileAccess.file_exists("user://" + service + "_creds.res"):
		
		DeckHolder.logger.log_system("No Credentials exist for " + service + " Service", Logger.LogType.WARN)
		return null
		
	
	var creds = ResourceLoader.load("user://" + service + "_creds.res")
	
	if not creds is CredSaver:
		
		DeckHolder.logger.log_system("Error loading Credentials for " + service + " Service", Logger.LogType.ERROR)
		return null
		
	
	return creds
	

## Temporary function for saving generic Connection credentials to user:// as a CredSaver
static func save_credentials(data : Dictionary, service: String):
	
	var creds := CredSaver.new()
	
	creds.data = data
	
	var err = ResourceSaver.save(creds, "user://" + service + "_creds.res")
	
	if err != OK:
		
		DeckHolder.logger.log_system("Error saving Credentials for " + service + " Service", Logger.LogType.ERROR)
		
	
