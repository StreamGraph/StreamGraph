# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
class_name StreamGraphConfig


static var config := {
	&"library_search_paths": [],
}

const SAVE_PATH := "user://config.json"


static func _static_init() -> void:
	var f := FileAccess.open(SAVE_PATH, FileAccess.READ)
	if not f:
		return
	
	var d = JSON.parse_string(f.get_as_text())
	config.merge(d, true)


static func get_p(property: StringName) -> Variant:
	return config.get(property)


static func has(property: StringName) -> bool:
	return config.has(property)


static func set_p(property: StringName, value: Variant) -> void:
	config[property] = value
	save()


static func get_dict() -> Dictionary:
	return config


static func merge(with: Dictionary) -> void:
	config.merge(with, true)
	save()


static func add_library_search_path(path: String) -> void:
	var arr: Array = config[&"library_search_paths"]
	arr.append(path)
	NodeDB.reload_libraries()
	save()


static func remove_library_search_path(path: String) -> void:
	var arr: Array = config[&"library_search_paths"]
	if arr.find(path) < 0:
		return
	arr.erase(path)
	NodeDB.reload_libraries()
	save()


static func rename_library_search_path(old_path: String, new_path: String) -> void:
	var arr: Array = config[&"library_search_paths"]
	var idx := arr.find(old_path)
	if idx < 0:
		return
	
	arr[idx] = new_path
	NodeDB.reload_libraries()
	save()


static func move_library_path_up(path: String) -> void:
	var arr: Array = config[&"library_search_paths"]
	var idx := arr.find(path)
	if idx < 0:
		return
	
	var old_path = arr[idx]
	arr[idx] = arr[idx - 1]
	arr[idx - 1] = old_path
	NodeDB.reload_libraries()
	save()


static func move_library_path_down(path: String) -> void:
	var arr: Array = config[&"library_search_paths"]
	var idx := arr.find(path)
	if idx < 0 or idx == arr.size() - 1:
		return
	
	var old_path = arr[idx]
	arr[idx] = arr[idx + 1]
	arr[idx + 1] = old_path
	NodeDB.reload_libraries()
	save()


static func get_library_search_paths() -> Array:
	var res := ["{$PWD}/library_groups"]
	res.append_array(config[&"library_search_paths"])
	return res


static func save() -> void:
	var f := FileAccess.open(SAVE_PATH, FileAccess.WRITE)
	if not f:
		DeckHolder.logger.log_system("Could not open config file for writing", Logger.LogType.ERROR)
		return
	
	f.store_string(JSON.stringify(config))
