# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends DeckNode

var group_id: String
var group_instance_id: String
var input_node: DeckNode
var output_node: DeckNode

var is_library: bool

var input_node_id: String
var output_node_id: String

var extra_ports: Array
var extra_port_values: Array:
	get:
		if extra_port_values.is_empty():
			return get_all_ports().map(func(x: Port): return x.value)
		else:
			return extra_port_values


func _init() -> void:
	name = "Group"
	node_type = "group_node"
	props_to_serialize = [
		&"group_id",
		&"group_instance_id",
		&"extra_ports",
		&"input_node_id",
		&"output_node_id",
		&"is_library",
		&"extra_port_values",
	]
	appears_in_search = false


#func _pre_port_load() -> void:
	#for port_type: PortType in extra_ports:
		#match port_type:
			#PortType.OUTPUT:
				#add_output_port(DeckType.Types.ANY, "Output %s" % get_output_ports().size())
			#PortType.INPUT:
				#add_input_port(DeckType.Types.ANY, "Input %s" % get_input_ports().size())


func init_io() -> void:
	#var group: Deck = _belonging_to.groups.get(group_id) as Deck
	var group := DeckHolder.get_group_instance(group_id, group_instance_id)
	if not group:
		return

	if input_node and input_node.ports_updated.is_connected(recalculate_ports):
		input_node.ports_updated.disconnect(recalculate_ports)
	if output_node and output_node.ports_updated.is_connected(recalculate_ports):
		output_node.ports_updated.disconnect(recalculate_ports)

	input_node = group.get_node(input_node_id)
	output_node = group.get_node(output_node_id)

	if input_node:
		input_node.about_to_free.connect(
			func():
				input_node_id = ""
		)
	if output_node:
		output_node.about_to_free.connect(
			func():
				output_node_id = ""
		)

	recalculate_ports()
	setup_connections()


func make_unique() -> Deck:
	return DeckHolder.make_group_instance_unique(group_id, group_instance_id, _belonging_to.id, _id)


func setup_connections() -> void:
	if input_node != null:
		input_node.ports_updated.connect(recalculate_ports)
	if output_node != null:
		output_node.ports_updated.connect(recalculate_ports)


func recalculate_ports() -> void:
	var _values := extra_port_values.duplicate()
	ports.clear()

	if output_node != null:
		for output_port: Port in output_node.get_input_ports().slice(0, output_node.get_input_ports().size() - 1):
			var port := add_output_port(
				output_port.type,
				output_port.label,
				output_port.descriptor,
				output_port.usage_type,
			)
			if _values.size() - 1 < port.index:
				continue
			
			port.set_value(_values[port.index])

	if input_node != null:
		for input_port: Port in input_node.get_output_ports().slice(0, input_node.get_output_ports().size() - 1):
			var port := add_input_port(
				input_port.type,
				input_port.label,
				input_port.descriptor,
				input_port.usage_type,
			)
			if _values.size() - 1 < port.index:
				continue
			
			port.set_value(_values[port.index])

	extra_ports.clear()
	extra_port_values.clear()
	for port in ports:
		extra_ports.append(port.port_type)


func _receive(to_input_port: int, data: Variant):
	# var i = DeckHolder.get_group_instance(group_id, group_instance_id).get_node(input_node_id)
	#i.send(get_input_ports()[to_input_port].index_of_type, data)
	input_node.send(get_input_ports()[to_input_port].index_of_type, data)


func _value_request(from_port: int) -> Variant:
	return await output_node.request_value_async(from_port)
