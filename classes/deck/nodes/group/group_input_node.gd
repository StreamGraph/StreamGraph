# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends DeckNode

var output_count: int:
	get:
		if output_count == 0:
			return get_all_ports().size()
		else:
			return output_count

var group_node: DeckNode


func _init() -> void:
	name = "Group input"
	node_type = "group_input"
	props_to_serialize = [&"output_count"]
	# appears_in_search = false
	# user_can_delete = false

	add_output_port(
		DeckType.Types.ANY,
		"Input 0"
	)
	outgoing_connection_added.connect(_on_outgoing_connection_added)
	outgoing_connection_removed.connect(_on_outgoing_connection_removed)


func _on_outgoing_connection_added(port_idx: int) -> void:
	if port_idx + 1 < get_all_ports().size():
		return

	add_output_port(
		DeckType.Types.ANY,
		"Input %s" % (get_all_ports().size())
	)


func _on_outgoing_connection_removed(port_idx: int) -> void:
	var last_connected_port := 0
	var outgoing_connections = _belonging_to.connections.get_all_outgoing_connections(_id)
	for port: int in outgoing_connections:
		if not (outgoing_connections[port] as Array).is_empty():
			last_connected_port = port
	#prints("l:", last_connected_port, "p:", port_idx)

	if port_idx < last_connected_port:
		return

	var s := get_all_ports().slice(0, last_connected_port + 2)
	ports.assign(s)
	ports_updated.emit()


func _pre_port_load() -> void:
	ports.clear()
	for i in output_count:
		add_output_port(
			DeckType.Types.ANY,
			"Input %s" % i
		)
	output_count = 0


func _post_load(connections: Deck.NodeConnections) -> void:
	# ensure we have enough ports after connections
	var last_connected_port := -1
	var outgoing_connections = connections.get_all_outgoing_connections(_id)
	for port: int in outgoing_connections:
		last_connected_port = port if outgoing_connections.has(port) else last_connected_port

	if last_connected_port == -1:
		return

	if ports.size() <= last_connected_port:
		for i in last_connected_port:
			add_output_port(
				DeckType.Types.ANY,
				"Input %s" % get_output_ports().size()
			)


func _post_deck_load() -> void:
	for port in get_output_ports():
		var gp := _belonging_to.group_descriptors.get_input_port(port.index_of_type)
		if gp.is_empty():
			continue
		port.label = gp.label if not gp.label.is_empty() else "Input %s" % port.index
		port.descriptor = gp.descriptor
		port.type = gp.type
		port.usage_type = gp.usage_type


func _value_request(from_port: int) -> Variant:
	if group_node:
		return await group_node.resolve_input_port_value_async(group_node.get_input_ports()[from_port].index_of_type)
		#return await group_node.request_value_async(group_node.get_input_ports()[from_port].index_of_type)
	else:
		return null
