# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends DeckNode

var input_count: int:
	get:
		if input_count == 0:
			return get_all_ports().size()
		else:
			return input_count

var group_node: DeckNode


func _init() -> void:
	name = "Group output"
	node_type = "group_output"
	props_to_serialize = [&"input_count"]
	# appears_in_search = false
	# user_can_delete = false

	add_input_port(
		DeckType.Types.ANY,
		"Output 0"
	)

	incoming_connection_added.connect(_on_incoming_connection_added)
	incoming_connection_removed.connect(_on_incoming_connection_removed)


func _on_incoming_connection_added(port_idx: int) -> void:
	if port_idx + 1 < get_all_ports().size():
		return

	add_input_port(
		DeckType.Types.ANY,
		"Output %s" % (get_all_ports().size())
	)


func _on_incoming_connection_removed(port_idx: int) -> void:
	var last_connected_port := 0
	var incoming_connections := _belonging_to.connections.get_all_incoming_connections(_id)
	if not incoming_connections.is_empty():
		last_connected_port = incoming_connections.keys()[-1]

	#prints("l:", last_connected_port, "p:", port_idx)

	if port_idx < last_connected_port:
		return

	var s := get_all_ports().slice(0, last_connected_port + 2)
	ports.assign(s)
	ports_updated.emit()


func _pre_port_load() -> void:
	ports.clear()
	for i in input_count:
		add_input_port(
			DeckType.Types.ANY,
			"Output %s" % i
		)
	input_count = 0


func _post_load(connections: Deck.NodeConnections) -> void:
	# ensure we have enough ports after connections
	var last_connected_port := 0
	var incoming_connections := connections.get_all_incoming_connections(_id)
	for port: int in incoming_connections:
		last_connected_port = port if incoming_connections.has(port) else last_connected_port

	if ports.size() <= last_connected_port:
		for i in last_connected_port:
			add_input_port(
				DeckType.Types.ANY,
				"Output %s" % get_input_ports().size()
			)


func _post_deck_load() -> void:
	for port in get_input_ports():
		var gp := _belonging_to.group_descriptors.get_output_port(port.index_of_type)
		if gp.is_empty():
			continue
		port.label = gp.label if not gp.label.is_empty() else "Output %s" % port.index
		port.descriptor = gp.descriptor
		port.type = gp.type
		port.usage_type = gp.usage_type


func _receive(to_input_port: int, data: Variant) -> void:
	if group_node:
		group_node.send(group_node.get_output_ports()[to_input_port].index_of_type, data)
