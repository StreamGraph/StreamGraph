# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends DeckNode


func _init() -> void:
	name = "Vector to OBS Position"
	node_type = "obs_vector_to_position"
	description = "Transforms a Vector into a position vector accepted by OBS transform inputs."

	add_input_port(
		DeckType.Types.DICTIONARY,
		"Vector"
	)
	add_output_port(
		DeckType.Types.DICTIONARY,
		"Position"
	)


func _value_request(_on_port: int) -> Variant:
	var v = await request_value_async(0)
	if not v:
		return null

	if not (v as Dictionary).has("x") or not (v as Dictionary).has("y"):
		return null

	return {"position_x": v.x, "position_y": v.y}


func _receive(_on_input_port: int, data: Variant) -> void:
	if not data is Dictionary:
		return
	
	if not (data as Dictionary).has("x") or not (data as Dictionary).has("y"):
		return
	
	send(0, {"position_x": data.x, "position_y": data.y})
