# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends DeckNode

var noobs: NoOBSWS


func _init() -> void:
	name = "OBS Is Source Visible?"
	node_type = name.to_snake_case()
	description = "Returns true if a source is visible in OBS."
	
	add_input_port(
		DeckType.Types.STRING,
		"Scene Name",
		"field",
	)
	
	add_input_port(
		DeckType.Types.NUMERIC,
		"Source ID",
		"spinbox:unbounded",
	)
	
	add_output_port(
		DeckType.Types.BOOL,
		"Visible",
	)


func _is_source_visible(scene_name: String, source_id: float) -> Variant:
	if noobs == null:
		noobs = Connections.obs_websocket

	var req := noobs.make_generic_request(
		"GetSceneItemEnabled",
		{
			"scene_name": scene_name,
			"scene_item_id": source_id,
		}
	)
	await req.response_received
	var data := req.message
	
	return data.response_data.scene_item_enabled


func _receive(to_input_port: int, data: Variant) -> void:
	if noobs == null:
		noobs = Connections.obs_websocket

	if to_input_port == 0:
		var source_id = await resolve_input_port_value_async(1)
		var scene_name = str(data)
		send(0, await _is_source_visible(scene_name, source_id))
	else:
		var scene_name = await resolve_input_port_value_async(0)
		var source_id = data
		send(0, await _is_source_visible(scene_name, source_id))


func _value_request(_on_output_port: int) -> Variant:
	var scene_name = await resolve_input_port_value_async(0)
	var source_id = await resolve_input_port_value_async(1)
	return await _is_source_visible(scene_name, source_id)
