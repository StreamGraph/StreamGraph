# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends DeckNode

var noobs: NoOBSWS


func _init() -> void:
	name = "Get Source Transform"
	node_type = "obs_get_source_transform"
	description = "Gets an OBS source's transform, which includes position and rotation, among other things."

	add_input_port(
		DeckType.Types.STRING,
		"Scene name",
		"field"
	)

	add_input_port(
		DeckType.Types.NUMERIC,
		"Source ID",
		"field"
	)
	
	add_output_port(
		DeckType.Types.DICTIONARY,
		"Transform",
		"",
	)


func get_xform(scene: String, source_id: float) -> Dictionary:
	if noobs == null:
		noobs = Connections.obs_websocket

	var req := noobs.make_generic_request(
		"GetSceneItemTransform",
		{
			"scene_name": scene,
			"scene_item_id": source_id,
		}
	)
	await req.response_received
	return req.message.response_data.scene_item_transform


func _receive(to_input_port: int, data: Variant) -> void:
	if to_input_port == 0:
		var source_id: float = await resolve_input_port_value_async(1)
		send(0, await get_xform(data, source_id))
	else:
		var scene: String = await resolve_input_port_value_async(0)
		send(0, await get_xform(scene, data))


func _value_request(_on_input_port: int) -> Variant:
	var scene: String = await resolve_input_port_value_async(0)
	var source_id: float = await resolve_input_port_value_async(1)
	return await get_xform(scene, source_id)
