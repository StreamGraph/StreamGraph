# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends DeckNode

var noobs: NoOBSWS

var lock := false # TODO: evaluate if this locking is actually needed

enum InputPorts{
	SCENE_NAME,
	SOURCE_ID,
	XFORM,
	SET,
}


func _init() -> void:
	name = "Set Source Transform"
	node_type = "obs_set_source_transform"
	description = "Sets an OBS source's transform, which includes position and rotation, among other things."

	props_to_serialize = []

	add_input_port(
		DeckType.Types.STRING,
		"Scene name",
		"field"
	)

	add_input_port(
		DeckType.Types.NUMERIC,
		"Source ID",
		"field"
	)

	add_input_port(
		DeckType.Types.DICTIONARY,
		"Transform"
	)

	add_input_port(
		DeckType.Types.ANY,
		"Set",
		"button",
		Port.UsageType.TRIGGER
	).button_pressed.connect(_receive.bind(InputPorts.SET, null))


func _receive(to_input_port: int, data: Variant) -> void:
#{ "scene_item_transform": { "alignment": 5, "bounds_alignment": 0, "bounds_height": 0, "bounds_type": "OBS_BOUNDS_NONE", "bounds_width": 0, "crop_bottom": 0, "crop_left": 0, "crop_right": 0, "crop_top": 0, "height": 257, "position_x": 1800, "position_y": 414, "rotation": 0, "scale_x": 1, "scale_y": 1, "source_height": 257, "source_width": 146, "width": 146 }}
	if noobs == null:
		noobs = Connections.obs_websocket

	var scene_name
	var source_id
	var xform
	
	match to_input_port:
		InputPorts.SCENE_NAME:
			scene_name = data
			source_id = await resolve_input_port_value_async(InputPorts.SOURCE_ID)
			xform = await resolve_input_port_value_async(InputPorts.XFORM)
		InputPorts.SOURCE_ID:
			source_id = data
			scene_name = await resolve_input_port_value_async(InputPorts.SCENE_NAME)
			xform = await resolve_input_port_value_async(InputPorts.XFORM)
		InputPorts.XFORM:
			xform = data
			source_id = await resolve_input_port_value_async(InputPorts.SOURCE_ID)
			scene_name = await resolve_input_port_value_async(InputPorts.SCENE_NAME)
		InputPorts.SET:
			xform = await resolve_input_port_value_async(InputPorts.XFORM)
			source_id = await resolve_input_port_value_async(InputPorts.SOURCE_ID)
			scene_name = await resolve_input_port_value_async(InputPorts.SCENE_NAME)

	if scene_name == null or str(scene_name).is_empty():
		return

	if source_id == null or not source_id is float:
		return

	if xform == null or not xform is Dictionary:
		return

	#if to_input_port != 3:
		#return
#
#
	#if lock:
		#return
#
	#var scene_name_req = await resolve_input_port_value_async(InputPorts.SCENE_NAME, "")
	#if scene_name_req == null:
		#return
#
	#var scene_name: String
#
	#if scene_name.is_empty():
		#return
#
	#var source_id_req = await resolve_input_port_value_async(InputPorts.SOURCE_ID)
	#if source_id_req == null:
		#return
	#
	#var source_id: float = source_id_req
#
	#var xform_req = await resolve_input_port_value_async(InputPorts.XFORM)
	#if xform_req == null:
		#return
#
	#var xform: Dictionary = xform_req
#
	#if xform.is_empty():
		#return
#
	lock = true
	var b := noobs.make_batch_request(false, NoOBSWS.Enums.RequestBatchExecutionType.SERIAL_FRAME)
	b.add_request(
		"SetSceneItemTransform",
		"",
		{
			"scene_name": scene_name,
			"scene_item_id": source_id,
			"scene_item_transform": xform,
		}
	)
	b.add_request("Sleep", "", {"sleep_frames": 1})
	b.response_received.connect(func(): lock = false)
	b.send()
