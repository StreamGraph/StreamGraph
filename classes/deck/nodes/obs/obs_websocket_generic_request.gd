# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends DeckNode

var noobs: NoOBSWS


func _init() -> void:
	name = "OBS WS Generic Request"
	node_type = "obs_generic_request"
	description = "Makes an OBS request and sends its result through. Use if if you really know what you're doing."

	props_to_serialize = []

	add_virtual_port(
		DeckType.Types.STRING,
		"Request type",
		"field"
	)

	add_virtual_port(
		DeckType.Types.STRING,
		"Request data",
		"codeblock"
	)

	add_output_port(
		DeckType.Types.DICTIONARY,
		"Result"
	)

	add_virtual_port(
		DeckType.Types.ANY,
		"Request",
		"button"
	).button_pressed.connect(request)


func request() -> void:
	if noobs == null:
		noobs = Connections.obs_websocket

	print(get_virtual_ports()[1].value)
	var e: Dictionary = type_convert(get_virtual_ports()[1].value, TYPE_DICTIONARY)
	print(e)
	if typeof(e) != TYPE_DICTIONARY:
		return

	var req := noobs.make_generic_request(
		get_virtual_ports()[0].value,
		str_to_var(get_virtual_ports()[1].value)
		)

	await req.response_received

	var d := req.message.get_data()
	send(0, d)
