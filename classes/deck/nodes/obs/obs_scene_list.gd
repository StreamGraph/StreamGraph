# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends DeckNode

var noobs: NoOBSWS


func _init() -> void:
	name = "Scene Selector"
	node_type = "obs_scene_list"
	description = ""

	props_to_serialize = []

	add_output_port(
		DeckType.Types.STRING,
		"Select a scene",
		"singlechoice",
		Port.UsageType.VALUE_REQUEST,
	)

	add_virtual_port(
		DeckType.Types.ANY,
		"Refresh",
		"button"
	).button_pressed.connect(refresh)


func refresh() -> void:
	if noobs == null:
		noobs = Connections.obs_websocket

	var items: Array
	var req := noobs.make_generic_request("GetSceneList")
	await req.response_received
	items = req.message.response_data.scenes
	var new_desc := "singlechoice"
	for scene in items:
		new_desc += ":" + scene.sceneName
	new_desc.trim_suffix(":")
	get_output_ports()[0].descriptor = new_desc
	ports_updated.emit()


func _value_request(_on_input_port: int) -> Variant:
	if ports[0].value != null:
		return ports[0].value

	return null
