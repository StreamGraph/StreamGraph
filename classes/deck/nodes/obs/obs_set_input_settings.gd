# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends DeckNode

var noobs: NoOBSWS


func _init() -> void:
	name = "OBS Set Input Settings"
	node_type = name.to_snake_case()
	description = "Sets settings on a source in OBS."
	aliases = ["source"]

	add_input_port(
		DeckType.Types.STRING,
		"Input/Source name",
		"field",
	)
	
	add_input_port(
		DeckType.Types.DICTIONARY,
		"Settings",
	)
	
	add_input_port(
		DeckType.Types.ANY,
		"Set",
		"button",
		Port.UsageType.TRIGGER,
	).button_pressed.connect(_receive.bind(2, null))



func _receive(to_input_port: int, data: Variant) -> void:
	if noobs == null:
		noobs = Connections.obs_websocket

	var input_name
	var settings

	match to_input_port:
		0:
			input_name = str(data)
			settings = await resolve_input_port_value_async(1)
		1:
			settings = data
			input_name = await resolve_input_port_value_async(0)
		2:
			input_name = await resolve_input_port_value_async(0)
			settings = await resolve_input_port_value_async(1)

	var _r := noobs.make_generic_request(
		"SetInputSettings",
		{
			"input_name": input_name,
			"input_settings": settings,
		}
	)
