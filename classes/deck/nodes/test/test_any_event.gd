# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends DeckNode


func _init() -> void:
	name = "Test Any Event"
	node_type = name.to_snake_case()

	add_input_port(
		DeckType.Types.STRING,
		"Event name",
		"field"
	)
	
	add_output_port(
		DeckType.Types.DICTIONARY,
		"Event",
		"",
		Port.UsageType.TRIGGER,
	)


func _event_received(event_name: StringName, data: Dictionary = {}) -> void:
	if event_name != await resolve_input_port_value_async(0):
		return
	
	send(0, data)
