# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends DeckNode


func _init() -> void:
	node_type = "test_interleaved"
	name = "Test Interleaved"

	for i in 4:
		add_output_port(
			DeckType.Types.STRING,
			"Test"
		)
		add_input_port(
			DeckType.Types.STRING,
			"Test"
		)
