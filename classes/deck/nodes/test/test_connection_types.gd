# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends DeckNode


func _init() -> void:
	name = "Test Usage"
	node_type = name.to_snake_case()

	add_input_port(
		DeckType.Types.ANY, "Both", "", Port.UsageType.BOTH
	)
	
	add_input_port(
		DeckType.Types.ANY, "Trigger", "", Port.UsageType.TRIGGER
	)
	
	add_input_port(
		DeckType.Types.ANY, "Value", "", Port.UsageType.VALUE_REQUEST
	)
	
	add_output_port(
		DeckType.Types.ANY, "Both", "", Port.UsageType.BOTH
	)
	
	add_output_port(
		DeckType.Types.ANY, "Trigger", "", Port.UsageType.TRIGGER
	)
	
	add_output_port(
		DeckType.Types.ANY, "Value", "", Port.UsageType.VALUE_REQUEST
	)
