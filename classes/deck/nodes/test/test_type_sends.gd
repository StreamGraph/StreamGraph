# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends DeckNode


func _init() -> void:
	name = "Test Type Sends"
	node_type = name.to_snake_case()
	description = ""

	add_output_port(DeckType.Types.BOOL, "bool", "checkbox", Port.UsageType.TRIGGER)
	add_output_port(DeckType.Types.STRING, "string", "field", Port.UsageType.TRIGGER)
	add_output_port(DeckType.Types.NUMERIC, "numeric", "spinbox:unbounded", Port.UsageType.TRIGGER)
	add_output_port(DeckType.Types.ARRAY, "array", "", Port.UsageType.TRIGGER)
	add_output_port(DeckType.Types.DICTIONARY, "dictionary", "", Port.UsageType.TRIGGER)
	add_output_port(DeckType.Types.DICTIONARY, "vector", "", Port.UsageType.TRIGGER)
	
	add_input_port(DeckType.Types.ANY, "Send", "button").button_pressed.connect(do)


func do() -> void:
	var id := UUID.v4()
	send(0, get_output_ports()[0].value, id)
	send(1, get_output_ports()[1].value, id)
	send(2, get_output_ports()[2].value, id)
	send(3, [1, 2, 3], id)
	send(4, {"hello": "world"}, id)
	send(5, {"x": 1, "y": 5}, id)

