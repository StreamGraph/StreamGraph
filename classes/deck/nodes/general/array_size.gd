# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends DeckNode


func _init() -> void:
	name = "Array Size"
	node_type = "array_size"
	description = "Returns the size of an array."

	aliases = ["length"]

	add_input_port(
		DeckType.Types.ARRAY,
		"Array",
	)
	
	add_output_port(
		DeckType.Types.NUMERIC,
		"Size",
	)


func _value_request(_on_output_port: int) -> Variant:
	var array: Array = await resolve_input_port_value_async(0)
	return array.size()


func _receive(_to_input_port: int, data: Variant) -> void:
	var array: Array = DeckType.convert_value(data, DeckType.Types.ARRAY)
	send(0, array.size())
