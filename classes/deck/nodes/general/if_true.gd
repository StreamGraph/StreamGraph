# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends DeckNode


func _init() -> void:
	name = "Pass If True"
	node_type = "if_true"
	description = "Pass input if and only if the condition input is true."

	add_input_port(
		DeckType.Types.BOOL,
		"Condition",
		"checkbox",
		Port.UsageType.VALUE_REQUEST,
	)
	add_input_port(
		DeckType.Types.ANY,
		"Input",
		"",
		Port.UsageType.TRIGGER,
	)

	add_output_port(
		DeckType.Types.ANY,
		"Output",
		"",
		Port.UsageType.TRIGGER,
	)


func _receive(to_input_port: int, data: Variant) -> void:
	if to_input_port != 1:
		return

	if await resolve_input_port_value_async(0):
		send(0, data)
