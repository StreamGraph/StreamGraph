# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends DeckNode


func _init() -> void:
	name = "Split String"
	node_type = "string_split"
	description = "Splits a string by a delimiter."

	add_input_port(
		DeckType.Types.STRING,
		"String",
		"field"
	)
	
	add_input_port(
		DeckType.Types.STRING,
		"Delimiter",
		"field"
	)
	
	add_output_port(
		DeckType.Types.ARRAY,
		"Result"
	)


func _value_request(_on_port: int) -> Variant:
	var delimiter = await resolve_input_port_value_async(1)
	if not delimiter:
		DeckHolder.logger.log_node("Split: could not resolve delimiter. Returning: []", Logger.LogType.ERROR)
		return []
	
	var string = await resolve_input_port_value_async(0)
	
	return Array(string.split(delimiter))


func _receive(on_input_port: int, data: Variant) -> void:
	if on_input_port == 0:
		if not data is String:
			DeckHolder.logger.log_node("Split: Received a value that's not String on port 0.", Logger.LogType.ERROR)
			return
		var delimiter = await resolve_input_port_value_async(1)
		
		send(0, Array(data.split(delimiter)))
	else:
		if not data is String:
			DeckHolder.logger.log_node("Split: Received a value that's not String on port 1 (delimiter).", Logger.LogType.ERROR)
			return
		
		var string = await resolve_input_port_value_async(0)
		
		send(0, Array(string.split(data)))
