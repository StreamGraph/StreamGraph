# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends DeckNode

var expr = Expression.new()


func _init():
	name = "Expression"
	node_type = "expression"
	description = "A node returning the result of a mathematical expression."

	# TODO: order of ports is changed
	# due to https://github.com/godotengine/godot/issues/85558
	# when it's fixed, switch it back
	add_input_port(
		DeckType.Types.ANY,
		"Expression Input",
	)

	add_output_port(
		DeckType.Types.ANY,
		"Expression Text",
		"codeblock",
	)


func parse(input: Variant) -> Variant:
	var text = get_output_ports()[0].value

	var err := expr.parse(text, ["deck_var", "input"])
	if err != OK:
		DeckHolder.logger.log_node("Expression parse failed: %s" % err, Logger.LogType.ERROR)
		return null

	var res = expr.execute([_belonging_to.variable_stack, input])
	if expr.has_execute_failed():
		DeckHolder.logger.log_node("Expression Execution Failed: %s" % expr.get_error_text(), Logger.LogType.ERROR)
		return null

	return res


func _value_request(_from_port : int) -> Variant:
	var input = await request_value_async(0)
	if input != null:
		return parse(input)
	else:
		return parse(null)


func _receive(_on_input_port: int, data: Variant) -> void:
	send(0, parse(data))
