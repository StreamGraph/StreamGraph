# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends DeckNode


func _init() -> void:
	name = "Get Dictionary Key"
	node_type = "dictionary_get_key"
	description = "Returns the value of a key from a dictionary input, if it exists, or null otherwise."

	add_input_port(
		DeckType.Types.DICTIONARY,
		"Dictionary"
	)

	add_input_port(
		DeckType.Types.STRING,
		"Key",
		"field"
	)

	add_output_port(
		DeckType.Types.ANY,
		"Value"
	)


func _value_request(_on_port: int) -> Variant:
	var d = await request_value_async(0)
	if d == null:
		return null

	var key = await resolve_input_port_value_async(1)
	if key == null:
		return null

	return d.get(key)


func _receive(on_input_port: int, data: Variant) -> void:
	if on_input_port == 0:
		if not data is Dictionary:
			DeckHolder.logger.log_node("Get Dictionary Key: Received data that's not dictionary on port 0.", Logger.LogType.ERROR)
			return
		var key = await resolve_input_port_value_async(1)
		if key == null:
			return
		
		send(0, (data as Dictionary).get(key))

	else:
		var d = await request_value_async(0)
		if d == null or (not d is Dictionary):
			DeckHolder.logger.log_node("Get Dictionary Key: Received data that's not dictionary on port 0.", Logger.LogType.ERROR)
			return
		
		send(0, d.get(data))
