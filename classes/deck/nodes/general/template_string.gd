# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends DeckNode


func _init() -> void:
	name = "String Template Format"
	node_type = "template_string"
	description = "Replaces variables in a string."

	add_input_port(
		DeckType.Types.STRING,
		"String to format",
		"field",
	)

	add_input_port(
		DeckType.Types.DICTIONARY,
		"Replacements",
	)
	
	add_output_port(
		DeckType.Types.STRING,
		"Result string",
	)


func _replace(string: String, replacements: Dictionary) -> String:
	return string.format(replacements)


func _value_request(_on_output_port: int) -> Variant:
	var string: String = DeckType.convert_value(await resolve_input_port_value_async(0), DeckType.Types.STRING)
	var replacements: Dictionary = DeckType.convert_value(await resolve_input_port_value_async(1), DeckType.Types.DICTIONARY)
	
	return _replace(string, replacements)


func _receive(on_input_port: int, data: Variant) -> void:
	if on_input_port == 0:
		var string: String = DeckType.convert_value(data, DeckType.Types.STRING)
		var replacements: Dictionary = DeckType.convert_value(await resolve_input_port_value_async(1), DeckType.Types.DICTIONARY)
		send(0, _replace(string, replacements))
	else:
		var string: String = DeckType.convert_value(await resolve_input_port_value_async(0), DeckType.Types.STRING)
		var replacements: Dictionary = DeckType.convert_value(data, DeckType.Types.DICTIONARY)
		send(0, _replace(string, replacements))
