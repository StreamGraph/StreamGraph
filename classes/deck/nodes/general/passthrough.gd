# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends DeckNode


func _init() -> void:
	name = "Passthrough"
	node_type = name.to_snake_case()
	description = "Receives a trigger input and passes it along, overwriting the data with the value from the second input."
	
	add_input_port(
		DeckType.Types.ANY,
		"Trigger",
		"",
		Port.UsageType.TRIGGER,
	)
	
	add_input_port(
		DeckType.Types.ANY,
		"Data",
		"field",
		Port.UsageType.VALUE_REQUEST,
	)
	
	add_output_port(
		DeckType.Types.ANY,
		"Output",
		"",
		Port.UsageType.TRIGGER,
	)


func _receive(to_input_port: int, _data: Variant) -> void:
	if to_input_port != 0:
		return

	var data = await resolve_input_port_value_async(1)
	send(0, data)
