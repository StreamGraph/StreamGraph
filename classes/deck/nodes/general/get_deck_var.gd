# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends DeckNode


func _init() -> void:
	name = "Get Deck Var"
	node_type = name.to_snake_case()
	description = "Retrieve a deck variable."

	add_output_port(
		DeckType.Types.ANY,
		"Variable",
		"field"
	)


func _value_request(_from_port: int) -> Variant:
	var key = ports[0].value
	return _belonging_to.variable_stack.get(key)
