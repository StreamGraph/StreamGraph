# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends DeckNode

var delta: float


func _init() -> void:
	name = "Process Loop"
	node_type = name.to_snake_case()
	description = "Sends a trigger output every frame, and returns the delta value (time since last frame in seconds)."

	add_input_port(
		DeckType.Types.BOOL,
		"Enabled",
		"checkbox",
		Port.UsageType.VALUE_REQUEST,
	)

	add_output_port(
		DeckType.Types.BOOL,
		"Trigger",
		"",
		Port.UsageType.TRIGGER
	)

	add_output_port(
		DeckType.Types.NUMERIC,
		"Delta",
		"",
	)


func _event_received(event_name: StringName, event_data: Dictionary = {}) -> void:
	if event_name != &"process":
		return

	var run = await resolve_input_port_value_async(0) == true

	if not run:
		return

	delta = event_data.delta
	var id := UUID.v4()
	send(0, true, id)
	send(1, delta, id)


func _value_request(on_output_port: int) -> Variant:
	if on_output_port != 1:
		return null

	return delta
