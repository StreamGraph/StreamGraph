# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends DeckNode


func _init() -> void:
	name = "Set Deck Var"
	node_type = name.to_snake_case()
	description = "Set a deck variable on trigger."

	add_input_port(
		DeckType.Types.STRING,
		"Variable Name",
		"field"
	)

	add_input_port(
		DeckType.Types.ANY,
		"Value",
		"field"
	)

	add_input_port(
		DeckType.Types.ANY,
		"Set",
		"button",
		Port.UsageType.TRIGGER,
	).button_pressed.connect(_receive.bind(2, null))

	add_output_port(
		DeckType.Types.ANY,
		"Value",
		"label",
		Port.UsageType.TRIGGER,
	)


func _receive(to_input_port: int, data: Variant) -> void:
	match to_input_port:
		0:
			if (not data is String) or (data as String).is_empty():
				DeckHolder.logger.log_node("Set Deck Var: received variable name that's not String or is empty.", Logger.LogType.ERROR)
				return
			var value: Variant = await resolve_input_port_value_async(1)
			_belonging_to.set_variable(data, value)
			send(0, value)
		1:
			var var_name = await resolve_input_port_value_async(0)
			if (not var_name is String) or (var_name as String).is_empty():
				DeckHolder.logger.log_node("Set Deck Var: variable name is null.")
				return
			_belonging_to.set_variable(var_name, data)
			send(0, data)
		2:
			var var_name = await resolve_input_port_value_async(0)
			
			var var_value: Variant = await resolve_input_port_value_async(1)

			#_belonging_to.variable_stack[var_name] = var_value
			_belonging_to.set_variable(var_name, var_value)

			send(0, var_value)
