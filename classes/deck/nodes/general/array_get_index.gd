# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends DeckNode


func _init() -> void:
	name = "Get Array Index"
	node_type = "array_get_index"
	description = "Returns an element from an array. 0-indexed."

	add_input_port(
		DeckType.Types.ARRAY,
		"Array"
	)
	
	add_input_port(
		DeckType.Types.NUMERIC,
		"Index",
		"spinbox:unbounded"
	)
	
	add_output_port(
		DeckType.Types.ANY,
		"Values"
	)


func _arr_index(arr: Array, index: int) -> Variant:
	if index < 0:
		index = (arr as Array).size() + index

	if index >= (arr as Array).size():
		DeckHolder.logger.log_node("Array index: Index is too big. Returning null.", Logger.LogType.ERROR)
		return null
	
	return arr[index]


func _value_request(_on_port: int) -> Variant:
	var arr = await request_value_async(0)
	if arr == null:
		DeckHolder.logger.log_node("Array index: Input array is null. Returning null.", Logger.LogType.ERROR)
		return null
	
	var idx := int(await resolve_input_port_value_async(1))

	return _arr_index(arr, idx)


func _receive(on_input_port: int, data: Variant) -> void:
	if on_input_port == 0:
		if not data is Array:
			DeckHolder.logger.log_node("Array Index: Received non-array on port 0.", Logger.LogType.ERROR)
			return
		
		var idx := int(await resolve_input_port_value_async(1))
		
		var res = _arr_index(data, idx)
		if res == null:
			return

		send(0, res)
		
	else:
		if (not data is int) or (not data is float):
			DeckHolder.logger.log_node("Array Index: Received non-number on port 1.", Logger.LogType.ERROR)
			return
		
		var arr = await request_value_async(0)
		if arr == null:
			DeckHolder.logger.log_node("Array index: Input array is null. Returning null.", Logger.LogType.ERROR)
			return
		
		var res = _arr_index(arr, int(data))
		if res == null:
			return

		send(0, res)
