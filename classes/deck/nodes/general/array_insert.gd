# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends DeckNode

enum InputPorts {
	ARRAY,
	INDEX,
	VALUE,
	INSERT,
}


func _init() -> void:
	name = "Array Insert At"
	node_type = "array_insert"
	description = "Insert a value into an array."

	add_input_port(
		DeckType.Types.ARRAY,
		"Array",
		"",
	)
	
	add_input_port(
		DeckType.Types.NUMERIC,
		"Index",
		"spinbox:unbounded",
	)
	
	add_input_port(
		DeckType.Types.ANY,
		"Value",
		"",
	)
	
	add_input_port(
		DeckType.Types.ANY,
		"Insert",
		"button",
		Port.UsageType.TRIGGER,
	).button_pressed.connect(_receive.bind(InputPorts.INSERT, null))
	
	add_output_port(
		DeckType.Types.ARRAY,
		"Array",
		"",
		Port.UsageType.TRIGGER,
	)


func _receive(to_input_port: int, data: Variant) -> void:
	var array: Array
	var value: Variant
	var index: int
	
	match to_input_port:
		InputPorts.ARRAY:
			array = data
			index = int(await resolve_input_port_value_async(InputPorts.INDEX))
			value = await resolve_input_port_value_async(InputPorts.VALUE)
		InputPorts.INDEX:
			array = await resolve_input_port_value_async(InputPorts.ARRAY)
			index = int(DeckType.convert_value(data, DeckType.Types.NUMERIC))
			value = await resolve_input_port_value_async(InputPorts.VALUE)
		InputPorts.VALUE:
			array = await resolve_input_port_value_async(InputPorts.ARRAY)
			index = int(await resolve_input_port_value_async(InputPorts.INDEX))
			value = data
		InputPorts.INSERT:
			array = await resolve_input_port_value_async(InputPorts.ARRAY)
			index = int(await resolve_input_port_value_async(InputPorts.INDEX))
			value = await resolve_input_port_value_async(InputPorts.VALUE)

	var err := array.insert(index, value)
	if err != OK:
		DeckHolder.logger.log_node("Array insert: Couldn't insert: %s." % error_string(err), Logger.LogType.ERROR)
		return
	
	send(0, array)
