# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends DeckNode

enum InputPorts {
	ARRAY,
	INDEX,
	POP,
}


func _init() -> void:
	name = "Array Pop Element"
	node_type = "array_pop"
	description = "Removes an element from an array and returns it. A negative index will remove from the end."

	aliases = ["remove", "erase"]

	add_input_port(
		DeckType.Types.ARRAY,
		"Array",
	)
	
	add_input_port(
		DeckType.Types.NUMERIC,
		"Element index",
		"spinbox:unbounded:1",
	).set_value(-1)
	
	add_input_port(
		DeckType.Types.ANY,
		"Pop",
		"button",
		Port.UsageType.TRIGGER,
	).button_pressed.connect(_receive.bind(InputPorts.POP, null))
	
	add_output_port(
		DeckType.Types.ARRAY,
		"Array",
		"",
		Port.UsageType.TRIGGER,
	)
	
	add_output_port(
		DeckType.Types.ANY,
		"Removed element",
		"",
		Port.UsageType.TRIGGER,
	)


func _pop(array: Array, index: int) -> Variant:
	return array.pop_at(index)


func _receive(to_input_port: int, data: Variant) -> void:
	var array: Array
	var index: int
	
	match to_input_port:
		InputPorts.ARRAY:
			array = data
			index = await resolve_input_port_value_async(InputPorts.INDEX)
		InputPorts.INDEX:
			array = await resolve_input_port_value_async(InputPorts.ARRAY)
			index = data
		InputPorts.POP:
			array = await resolve_input_port_value_async(InputPorts.ARRAY)
			index = await resolve_input_port_value_async(InputPorts.INDEX)

	var send_id := UUID.v4()
	var value = _pop(array, index)
	send(0, array, send_id)
	send(1, value, send_id)
