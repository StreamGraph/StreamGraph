# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends DeckNode


func _init() -> void:
	name = "Compare Values"
	node_type = "is_equal"
	description = "Returns true if the values are equal, false otherwise."

	add_input_port(
		DeckType.Types.ANY,
		"Value A",
		"field"
	)

	add_input_port(
		DeckType.Types.ANY,
		"Value B",
		"field"
	)

	add_output_port(
		DeckType.Types.BOOL,
		"Result"
	)


func _value_request(_on_port: int) -> Variant:
	var a = await resolve_input_port_value_async(0)
	var b = await resolve_input_port_value_async(1)
	
	return a == b


func _receive(on_port: int, data: Variant) -> void:
	if on_port == 0:
		var b = await resolve_input_port_value_async(1)
		send(0, data == b)
	else:
		var b = await resolve_input_port_value_async(0)
		send(0, data == b)
