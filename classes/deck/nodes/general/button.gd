# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends DeckNode


func _init() -> void:
	name = "Button"
	node_type = "button"
	description = "A button to trigger certain nodes that have a trigger input."

	var port := add_output_port(
		DeckType.Types.ANY,
		"Press me",
		"button",
		Port.UsageType.TRIGGER,
	)
	
	port.button_pressed.connect(send.bind(port.index_of_type, null))
