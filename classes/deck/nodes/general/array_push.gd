# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends DeckNode

enum InputPorts {
	ARRAY,
	VALUE,
	APPEND,
}


func _init() -> void:
	name = "Array Append Element"
	node_type = "array_push"
	description = "Inserts an element at the end of an array."
	aliases = ["push"]
	
	add_input_port(
		DeckType.Types.ARRAY,
		"Array",
	)
	
	add_input_port(
		DeckType.Types.ANY,
		"Value",
		"field",
	)
	
	add_input_port(
		DeckType.Types.ANY,
		"Append",
		"button",
		Port.UsageType.TRIGGER,
	).button_pressed.connect(_receive.bind(InputPorts.APPEND, null))
	
	add_output_port(
		DeckType.Types.ARRAY,
		"Array",
		"",
		Port.UsageType.TRIGGER,
	)


func _do(array: Array, element: Variant) -> Array:
	array.append(element)
	return array


func _receive(to_input_port: int, data: Variant) -> void:
	var array: Array
	var element: Variant
	
	match to_input_port:
		InputPorts.ARRAY:
			array = data
			element = await resolve_input_port_value_async(InputPorts.VALUE)
		InputPorts.VALUE:
			array = await resolve_input_port_value_async(InputPorts.ARRAY)
			element = data
		InputPorts.APPEND:
			array = await resolve_input_port_value_async(InputPorts.ARRAY)
			element = await resolve_input_port_value_async(InputPorts.VALUE)
	
	send(0, _do(array, element))
