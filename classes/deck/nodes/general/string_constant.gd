# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends DeckNode


func _init() -> void:
	node_type = "string_constant"
	name = "String Constant"
	description = "A String field."

	add_output_port(
		DeckType.Types.STRING,
		"Text",
		"field",
		Port.UsageType.VALUE_REQUEST,
	)

func _value_request(_from_port: int) -> Variant:
	return ports[0].value
