# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends DeckNode


func _init():
	name = "Twitch Connected Account Info"
	node_type = name.to_snake_case()
	description = "Returns the data for the connected Twitch account."

	props_to_serialize = []
	
	add_output_port(DeckType.Types.STRING, "Login", "", Port.UsageType.VALUE_REQUEST)
	add_output_port(DeckType.Types.STRING, "Display Name", "", Port.UsageType.VALUE_REQUEST)
	add_output_port(DeckType.Types.STRING, "User ID", "", Port.UsageType.VALUE_REQUEST)
	add_output_port(DeckType.Types.DICTIONARY, "User Dictionary", "", Port.UsageType.VALUE_REQUEST)
	
#{
	  #"id": "141981764",
	  #"login": "twitchdev",
	  #"display_name": "TwitchDev",
	  #"type": "",
	  #"broadcaster_type": "partner",
	  #"description": "Supporting third-party developers building Twitch integrations from chatbots to game integrations.",
	  #"profile_image_url": "https://static-cdn.jtvnw.net/jtv_user_pictures/8a6381c7-d0c0-4576-b179-38bd5ce1d6af-profile_image-300x300.png",
	  #"offline_image_url": "https://static-cdn.jtvnw.net/jtv_user_pictures/3f13ab61-ec78-4fe6-8481-8682cb3b0ac2-channel_offline_image-1920x1080.png",
	  #"view_count": 5980557,
	  #"email": "not-real@email.com",
	  #"created_at": "2016-12-14T20:32:28Z"
#}


func _value_request(from_port):
	
	var user_dict = Connections.twitch.user_info
	
	if user_dict.is_empty():
		
		DeckHolder.logger.log_node("Twitch Connection: No Data Cached for the connected Twitch Account, please try Connecting again", Logger.LogType.ERROR)
		return null
		
	
	match from_port:
			
			0: # Login Name
				
				return user_dict.login
				
			
			1: # Display Name
				
				return user_dict.display_name
				
			
			2: # Profile Picture URL
				
				return user_dict.id
				
			
			_: # Dictionary
				
				return user_dict
				
			
		
	
	return Connections.twitch.user_info
	


