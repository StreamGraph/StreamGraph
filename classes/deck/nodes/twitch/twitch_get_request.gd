# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends DeckNode


enum inputs {
	url,
	params,
	trigger
}

func _init():
	name = "Twitch Get Request"
	node_type = name.to_snake_case()
	description = "A simple node for doing an HTTP Request to Twitch using the Authorized Account Information"
	
	add_input_port(DeckType.Types.STRING, "Request URL", "field")
	add_input_port(DeckType.Types.DICTIONARY, "Parameters")
	add_input_port(DeckType.Types.ANY, "Trigger")
	
	add_output_port(DeckType.Types.DICTIONARY, "Response", "", Port.UsageType.TRIGGER)
	


func _receive(to_input_port, data: Variant):
	
	if (to_input_port == 0 or to_input_port == 1) and data == null:
		
		return
		
	
	var params : Dictionary
	var url : String
	
	match to_input_port:
		
		inputs.url:
			
			url = str(data)
			params = await resolve_input_port_value_async(inputs.params)
			
		inputs.params:
			
			if not data is Dictionary:
				return
			
			params = data
			url = await resolve_input_port_value_async(inputs.url)
			
		inputs.trigger:
			
			url = await resolve_input_port_value_async(inputs.url)
			params = await resolve_input_port_value_async(inputs.params)
			
		
	
	if url == null or not url.begins_with("https://api.twitch.tv/helix/"):
		return
	
	var twitch := Connections.twitch
	var url_string := url
	
	if not params.is_empty() and not url_string.ends_with("?"):
		
		url_string += "?"
		
	
	for key in params:
		
		var val = params[key]
		
		url_string += key + "=" + val + "&"
		
	
	url_string = url_string.trim_suffix("&")
	
	var resp := twitch.twitch_request(url_string)
	
	resp.response_received.connect(func(info):
		
		info.erase("headers")
		send(0, info)
		)
	
