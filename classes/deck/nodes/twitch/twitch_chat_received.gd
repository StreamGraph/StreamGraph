# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends DeckNode

var username := ""
var message := ""
var channel := ""
var tags := {}


func _init():
	name = "Twitch Chat Received"
	node_type = "twitch_chat_received"
	description = "Receives Twitch chat events from a Twitch connection."

	add_output_port(
		DeckType.Types.ANY,
		"On receive",
		"",
		Port.UsageType.TRIGGER,
	)
	add_output_port(DeckType.Types.STRING, "Username")
	add_output_port(DeckType.Types.STRING, "Message")
	add_output_port(DeckType.Types.STRING, "Channel")
	add_output_port(DeckType.Types.DICTIONARY, "Tags")




func _event_received(event_name : StringName, event_data : Dictionary = {}):
	
	if event_name != &"twitch_chat":
		
		return
		
	username = event_data.username
	message = event_data.message
	channel = event_data.channel
	tags = event_data
	
	var id = UUID.v4()
	
	send(0, null, id)
	send(1, username, id)
	send(2, message, id)
	send(3, channel, id)
	send(4, tags, id)


func _value_request(on_port: int) -> Variant:
	match on_port:
		1:
			return username
		2:
			return message
		3:
			return channel
		4:
			return tags
		0, _:
			return null
		
