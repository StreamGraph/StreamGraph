# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends DeckNode


func _init() -> void:
	name = "PI Constant"
	node_type = "pi_constant"
	description = "The Pi constant."

	add_output_port(
		DeckType.Types.NUMERIC,
		"π Value",
		"",
		Port.UsageType.VALUE_REQUEST
	)


func _value_request(_on_output_port: int) -> float:
	return PI
