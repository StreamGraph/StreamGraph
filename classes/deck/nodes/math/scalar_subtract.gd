# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends DeckNode


func _init() -> void:
	name = "Subtract Numbers"
	node_type = "scalar_subtract"
	description = "Subtracts one number from another."

	add_input_port(
		DeckType.Types.NUMERIC,
		"Number A",
		"spinbox:unbounded:0.0001",
	)
	
	add_input_port(
		DeckType.Types.NUMERIC,
		"Number B",
		"spinbox:unbounded:0.0001",
	)
	
	add_output_port(
		DeckType.Types.NUMERIC,
		"Result"
	)


func _value_request(_on_output_port: int) -> Variant:
	var va = await resolve_input_port_value_async(0)
	var vb = await resolve_input_port_value_async(1)
	
	va = DeckType.convert_value(va, DeckType.Types.NUMERIC)
	vb = DeckType.convert_value(vb, DeckType.Types.NUMERIC)
	
	return va - vb


func _receive(on_input_port: int, data: Variant) -> void:
	var va
	var vb
	if on_input_port == 0:
		va = data
		vb = await resolve_input_port_value_async(1)
	else:
		va = await resolve_input_port_value_async(0)
		vb = data

	va = DeckType.convert_value(va, DeckType.Types.NUMERIC)
	vb = DeckType.convert_value(vb, DeckType.Types.NUMERIC)
	
	send(0, va - vb)
