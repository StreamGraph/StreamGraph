# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends DeckNode


func _init() -> void:
	name = "Decompose Vector"
	node_type = "vector_decompose"
	description = "Returns the X and Y components of a vector."

	add_input_port(
		DeckType.Types.DICTIONARY,
		"Vector"
	)

	add_output_port(
		DeckType.Types.NUMERIC,
		"X"
	)
	add_output_port(
		DeckType.Types.NUMERIC,
		"Y"
	)


func _receive(_port : int, data : Variant) -> void:
	
	if !data or !DeckType.is_valid_vector(data):
		DeckHolder.logger.log_node("Vector Decompose: the vector is invalid.", Logger.LogType.ERROR)
		return
		
	
	var id = UUID.v4()
	send(0, data.x, id)
	send(1, data.y, id)
	

func _value_request(on_port: int) -> Variant:
	var v = await request_value_async(0)
	if not v:
		DeckHolder.logger.log_node("Vector Decompose: the vector is invalid.", Logger.LogType.ERROR)
		return null

	if !DeckType.is_valid_vector(v):
		DeckHolder.logger.log_node("Vector Decompose: the vector is invalid.", Logger.LogType.ERROR)
		return null

	if on_port == 0:
		return v.x
	else:
		return v.y
