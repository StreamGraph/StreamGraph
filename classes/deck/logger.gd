# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
class_name Logger

enum LogType {
	INFO,
	WARN,
	ERROR,
}

enum LogCategory {
	NODE,
	DECK,
	SYSTEM,
	RENDERER,
}

# Dictionary["text": String, "type": LogType]
var toast_history: Array[Dictionary]

signal log_message(text: String, type: LogType, category: LogCategory)
signal log_toast(text: String, type: LogType)


func log_node(text: Variant, type: LogType = LogType.INFO) -> void:
	self.log(str(text), type, LogCategory.NODE)


func log_deck(text: Variant, type: LogType = LogType.INFO) -> void:
	self.log(str(text), type, LogCategory.DECK)


func log_system(text: Variant, type: LogType = LogType.INFO) -> void:
	self.log(str(text), type, LogCategory.SYSTEM)


func log_renderer(text: Variant, type: LogType = LogType.INFO) -> void:
	self.log(str(text), type, LogCategory.RENDERER)


func log(text: String, type: LogType, category: LogCategory) -> void:
	log_message.emit(text, type, category)
	
	if OS.has_feature("editor"):
		prints(LogType.keys()[type].capitalize(), LogCategory.keys()[category].capitalize(), text)


func toast_info(text: String) -> void:
	toast(text, LogType.INFO)


func toast_warn(text: String) -> void:
	toast(text, LogType.WARN)


func toast_error(text: String) -> void:
	toast(text, LogType.ERROR)


func toast(text: String, type: LogType) -> void:
	toast_history.append(
		{
			"text": text,
			"type": type,
		}
	)
	log_toast.emit(text, type)
	if OS.has_feature("editor"):
		prints("(t)", LogType.keys()[type].capitalize(), text)
