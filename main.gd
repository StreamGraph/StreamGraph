# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends Node

const DEFAULT_RENDERER := preload("res://graph_node_renderer/deck_holder_renderer.tscn")

var deck_holder_renderer: DeckHolderRenderer = null
var deck_holder_renderer_finished := false


func _ready() -> void:
	get_tree().auto_accept_quit = false
	
	if DisplayServer.get_name() != "headless":
		# not headless, start default renderer
		deck_holder_renderer = DEFAULT_RENDERER.instantiate()
		add_child(deck_holder_renderer)
		deck_holder_renderer.quit_completed.connect(_on_deck_holder_renderer_quit_completed)


func _on_deck_holder_renderer_quit_completed() -> void:
	# will be used later to make sure both default and rpc are finished processing
	deck_holder_renderer_finished = true
	
	DeckHolder.pre_exit_cleanup()
	get_tree().quit()


func _notification(what: int) -> void:
	if what == NOTIFICATION_WM_CLOSE_REQUEST:
		if deck_holder_renderer:
			deck_holder_renderer.request_quit()
