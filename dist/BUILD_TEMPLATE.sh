#!/bin/bash

# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

# Template script to build StreamGraph for Linux and Windows targets.
# While it can be invoked manually, it's part of a series of automation scripts.
# This script is tailor-made for one system and may not necessarily work on others.
# If you want a safer way to build StreamGraph, use Godot's Export menu.

# This script is copied to a version folder as part of the build process.
# It will not try detecting that the folder it's in is valid, so do not run it if it's named "BUILD_TEMPLATE.sh".
# It expects a "godot" executable to be present in PATH.

# If the first argument is "clean", the script will remove the built executables in the linux/ and windows/ folders and zip files it creates.

target=`pwd`
version=`basename $target`
filename="StreamGraph-v$version"

# Build StreamGraph for linux and windows and zip them up.
function build {
  if ! command -v godot &> /dev/null; then
    echo "ERROR: godot not found in PATH. Aborting."
    exit 1
  fi

  cd ../../
  godot --headless --quiet --export-debug "linux" "$target/linux/$filename.x86_64"
  godot --headless --quiet --export-debug "windows" "$target/windows/$filename.exe"

  cd "$target"
  zip -rq "$filename-linux.zip" linux
  zip -rq "$filename-windows.zip" windows
  exit 0
}

# Clean the linux and windows folders and zip files.
function clean {
  rm -f windows/*
  rm -f linux/*
  rm -f "$filename-linux.zip"
  rm -f "$filename-windows.zip"
  exit 0
}

if [[ $1 = "clean" ]]; then
  clean
else
  build
fi
