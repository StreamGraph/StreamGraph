#!/bin/bash

# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

# Helper script to prepare folders for a StreamGraph release.
# Takes one argument, the version string, and creates a relevant folder structure, copying the build template into the version folder.

if [[ $# -ne 1 ]]; then
  echo "ERROR: Version string required."
  exit 2
fi

if [[ -d $1 ]]; then
  echo "WARN: The directory for version $1 already exists."
  cp BUILD_TEMPLATE.sh "$1/build.sh"
  exit 1
fi

mkdir -p "$1"/linux
mkdir -p "$1"/windows

cp BUILD_TEMPLATE.sh "$1/build.sh"
