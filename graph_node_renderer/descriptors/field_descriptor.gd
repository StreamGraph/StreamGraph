# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends DescriptorContainer

@onready var line_edit: LineEdit = %LineEdit


func _setup(port: Port, node: DeckNode) -> void:
	if port.value:
		line_edit.text = str(port.value)
	line_edit.placeholder_text = port.label
	port.value_callback = line_edit.get_text
	line_edit.text_changed.connect(port.set_value)
	line_edit.editable = not node._belonging_to.is_library


func set_value(new_value: Variant) -> void:
	if line_edit.has_focus():
		return
	
	line_edit.text = new_value
