# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends DescriptorContainer

@onready var button: Button = %Button


func _setup(port: Port, node: DeckNode) -> void:
	button.text = port.label
	button.pressed.connect(node.press_button.bind(port.index))
	button.disabled = node._belonging_to.is_library
