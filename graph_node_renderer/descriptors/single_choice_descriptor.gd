# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends DescriptorContainer

@onready var box: OptionButton = %Box


func _setup(port: Port, node: DeckNode) -> void:
	if descriptor.slice(1).is_empty():
		if port.value:
			box.add_item(port.value)
		else:
			box.add_item(port.label)
	else:
		for item in descriptor.slice(1):
			box.add_item(item)
	port.value_callback = func(): return box.get_item_text(box.get_selected_id())
	if port.type == DeckType.Types.STRING:
		box.item_selected.connect(
			func(_id: int):
				port.set_value.call(box.get_item_text(box.get_selected_id()))
		)

		if port.value != null:
			for i in box.get_item_count():
				if box.get_item_text(i) == port.value:
					box.select(i)
					break
	box.disabled = node._belonging_to.is_library

func set_value(new_value: Variant) -> void:
	if box.has_focus():
		return
	
	for i in box.item_count:
		if box.get_item_text(i) == new_value:
			box.select(i)
			break
