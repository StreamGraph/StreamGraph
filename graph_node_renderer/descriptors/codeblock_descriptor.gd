# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends DescriptorContainer

@onready var code_edit: CodeEdit = %CodeEdit


func _setup(port: Port, node: DeckNode) -> void:
	if port.value:
		code_edit.text = str(port.value)
	code_edit.placeholder_text = port.label
	port.value_callback = code_edit.get_text
	code_edit.text_changed.connect(port.set_value.bind(code_edit.get_text))
	code_edit.custom_minimum_size = Vector2(200, 100)
	code_edit.size_flags_vertical = SIZE_EXPAND_FILL
	code_edit.editable = not node._belonging_to.is_library


func set_value(new_value: Variant) -> void:
	if code_edit.has_focus():
		return
	
	code_edit.text = new_value
