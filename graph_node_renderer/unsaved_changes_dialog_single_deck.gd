# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends ConfirmationDialog
class_name UnsavedChangesDialogSingleDeck


func _init() -> void:
	add_button("Don't Save", true, "force_close")
