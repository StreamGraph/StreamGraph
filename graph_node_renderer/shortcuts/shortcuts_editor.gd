# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends VBoxContainer
class_name ShortcutsEditor
## An editor for [RendererShortcuts].

const REVERT_ICON = preload("res://graph_node_renderer/textures/revert-icon.svg")


func _notification(what: int) -> void:
	if what == RendererShortcuts.NOTIFICATION_SHORTCUTS_LOADED:
		var g := ButtonGroup.new()
		var map := RendererShortcuts.get_full_map()
		for action: String in map:
			if action.begins_with("_sep"):
				# draw a separator.
				var l := Label.new()
				l.text = map[action].capitalize()
				
				var hb := HBoxContainer.new()
				var sep := HSeparator.new()
				sep.size_flags_horizontal = Control.SIZE_EXPAND_FILL
				hb.add_child(sep)
				hb.add_child(l)
				hb.add_child(sep.duplicate(0))
				
				add_child.call_deferred(hb)
				continue

			var c := ShortcutDisplay.new(action, action.capitalize(), g)
			add_child.call_deferred(c)


## Unfocuses all [ShorcutsEditor.ShortcutDisplay] children.
func ensure_no_focus() -> void:
	for c in get_children():
		if c is ShortcutDisplay:
			c.unfocus_button()


## A container for displaying an editable shorcut.
class ShortcutDisplay extends HBoxContainer:
	## The action this container represents and edits.
	var action: String
	var _label: Label
	var _button: ShortcutButton
	var _reset_button: Button
	var _cc: CenterContainer
	
	
	func _init(p_action: String, p_label: String, p_group: ButtonGroup) -> void:
		action = p_action
		
		_label = Label.new()
		_label.size_flags_horizontal = Control.SIZE_EXPAND_FILL
		_label.text = p_label
		
		_button = ShortcutButton.new(RendererShortcuts.get_shortcut(p_action).events[0])
		_button.button_group = p_group
		_button.text = RendererShortcuts.get_shortcut(p_action).get_as_text()
		
		_button.confirmed.connect(
			func(raw_event: InputEvent):
				var override := RendererShortcuts.add_override(action, raw_event)
				_button.set_event(override.events[0])
				_reset_button.disabled = false
		)
		
		_reset_button = Button.new()
		_reset_button.icon = REVERT_ICON
		_reset_button.tooltip_text = "Reset (default: %s)" % RendererShortcuts.get_default(action).get_as_text()
		
		_reset_button.pressed.connect(
			func():
				_button.set_event(RendererShortcuts.get_default(action).events[0])
				_reset_button.disabled = true
				RendererShortcuts.remove_override(action)
		)
		_reset_button.disabled = not RendererShortcuts.has_override(action)
		
		_cc = CenterContainer.new()
		_cc.size_flags_horizontal = Control.SIZE_EXPAND_FILL
		_button.size_flags_horizontal = Control.SIZE_SHRINK_CENTER
		
		add_child(_label)
		_cc.add_child(_button)
		add_child(_cc)
		add_child(_reset_button)
	
	
	## Removes focus of the container's button, in cases where focus is
	## about to be lost to prevent remapping an action on accident.
	func unfocus_button() -> void:
		_button.set_event(_button._event)


## A button that displays a shortcut and allows editing (remapping) it.
class ShortcutButton extends Button:
	var _event: InputEvent
	var _prev_event: InputEvent

	## Emitted when Enter is pressed on the button.
	signal confirmed(raw_event: InputEvent)


	func _init(p_event: InputEvent) -> void:
		toggle_mode = true
		set_event(p_event)
		button_down.connect(
			func():
				text += "..."
		)
		
		toggled.connect(
			func(toggled_on: bool):
				if not toggled_on:
					text = _event.as_text()
		)
	
	
	## Sets the event this button is storing and updates its text.
	func set_event(p_event: InputEvent) -> void:
		button_pressed = false
		_event = p_event
		text = _event.as_text()
	
	
	func _unhandled_key_input(event: InputEvent) -> void:
		if not button_pressed:
			return
		
		accept_event()
		if event.keycode == KEY_ESCAPE:
			set_event(_event)
			return
		
		if event.keycode == KEY_ENTER:
			button_pressed = false
			confirmed.emit(_prev_event)
			return

		if event.is_pressed():
			text = "%s..." % event.as_text()
			_prev_event = event
		
