# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends PanelContainer
@onready var h_flow_container: HFlowContainer = %HFlowContainer


func _ready() -> void:
	for i in NoOBSWS.Enums.EventSubscription.keys():
		if NoOBSWS.Enums.EventSubscription[i] == NoOBSWS.Enums.EventSubscription.ALL:
			continue
		
		if NoOBSWS.Enums.EventSubscription[i] == NoOBSWS.Enums.EventSubscription.NONE:
			continue
		
		var cb := CheckBox.new()
		cb.text = i.capitalize()
		cb.set_meta(&"bit", NoOBSWS.Enums.EventSubscription[i])
		#cb.toggled.connect(check_toggled.bind(NoOBSWS.Enums.EventSubscription[i]))
		h_flow_container.add_child(cb)


func get_subscriptions() -> int:
	var res := 0
	for i: CheckBox in h_flow_container.get_children():
		if i.button_pressed:
			res |= i.get_meta(&"bit")
	return res


func set_subscriptions(subscriptions: int) -> void:
	for i: CheckBox in h_flow_container.get_children():
		i.button_pressed = subscriptions & i.get_meta(&"bit") == i.get_meta(&"bit")
