# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
@tool
extends VBoxContainer
class_name AccordionMenu
## A collapsible menu container.
##
## Provides a container that arranges its children vertically with a button to collapse
## and uncollapse its children. The name of the node controls the collapse button's text.

## Controls whether children are visible.
@export var collapsed: bool = false:
	set = set_collapsed

## If [code]true[/code], the menu will draw a background under its children,
## which can be helpful for telling nested elements apart.[br]
## The stylebox for drawing this background can be defined in the [Theme],
## which is called [code]background[/code].
@export var draw_background: bool = false:
	set(v):
		draw_background = v
		queue_redraw()

## If [code]true[/code], the menu will draw lines pointing to child [Control]s,
## similar to [Tree].
@export var draw_tree: bool = true:
	set(v):
		draw_tree = v
		queue_redraw()

var _collapse_button: CollapseButton
const COLLAPSE_ICON = preload("res://graph_node_renderer/textures/collapse-icon.svg")
const COLLAPSE_ICON_COLLAPSED = preload("res://graph_node_renderer/textures/collapse-icon-collapsed.svg")
const BASE_MARGIN := 12
var _indent_level := 1
var _ignore_child_lines: Array[int]
var _title: String

var _renamed_lambda = func():
	_set_title(name)

signal menu_collapsed(is_visible: bool)


func _enter_tree() -> void:
	if get_child_count(true) > 0 and get_child(0, true) is CollapseButton:
		get_child(0, true).queue_free()
	
	_collapse_button = CollapseButton.new(collapsed, name)
	_collapse_button.toggled.connect(set_collapsed)
	
	if _title.is_empty():
		renamed.connect(_renamed_lambda)
	else:
		_collapse_button.text = _title
	
	add_child(_collapse_button, false, Node.INTERNAL_MODE_FRONT)


func _exit_tree() -> void:
	_collapse_button.queue_free()


func _set_title(title: String) -> void:
	_title = title
	
	if _collapse_button:
		_collapse_button.text = title


func set_title(title: String) -> void:
	if renamed.is_connected(_renamed_lambda):
		renamed.disconnect(_renamed_lambda)
	
	_set_title(title)


func set_collapsed(v: bool) -> void:
	collapsed = v
	if _collapse_button:
		_collapse_button.button_pressed = collapsed
	if collapsed:
		await collapse()
	else:
		await uncollapse()
	queue_redraw()


## Collapse the menu, making the children invisible.
func collapse() -> void:
	if _collapse_button:
		_collapse_button.icon = COLLAPSE_ICON_COLLAPSED
	for child in get_children(false):
		child.visible = false
	if is_inside_tree():
		menu_collapsed.emit(true)
		await get_tree().process_frame
		update_minimum_size()


## Uncollapse the menu, making the children visible.
func uncollapse() -> void:
	if _collapse_button:
		_collapse_button.icon = COLLAPSE_ICON
	for child in get_children(false):
		child.visible = true
	if is_inside_tree():
		menu_collapsed.emit(false)
		await get_tree().process_frame
		update_minimum_size()


func set_ignore_child_lines(idx: int, ignore: bool = true) -> void:
	if idx not in _ignore_child_lines and ignore:
		_ignore_child_lines.append(idx)
		return
	
	if idx in _ignore_child_lines and not ignore:
		_ignore_child_lines.erase(idx)
		return


func _notification(what: int) -> void:
	if what == NOTIFICATION_PRE_SORT_CHILDREN:
		for i in get_children(false):
			i.visible = not collapsed
	
	if what == NOTIFICATION_SORT_CHILDREN:
		for child: Control in get_children(false):
			var base_rect := child.get_rect()
			base_rect.size.x -= BASE_MARGIN * _indent_level
			base_rect.position.x += BASE_MARGIN * _indent_level
			fit_child_in_rect(child, base_rect)


func _draw() -> void:
	if draw_tree and not collapsed and get_child_count() > 0:
		# draw a tree-like line.
		var line_color := Color(1.0, 1.0, 1.0, 0.35)
		# first, draw a single vertical line
		var line_start_x := _collapse_button.position.x + BASE_MARGIN / 2.0
		var tree_line_start := Vector2(line_start_x, _collapse_button.position.y + _collapse_button.size.y)
		var tree_line_end := tree_line_start
		#tree_line_end.y = get_rect().size.y
		tree_line_end.y = get_combined_minimum_size().y
		tree_line_end.y -= get_child(-1, false).size.y / 2.0
		
		if get_child(-1, false) is AccordionMenu:
			var other: AccordionMenu = get_child(-1, false) as AccordionMenu
			tree_line_end.y -= other.size.y / 2
			tree_line_end.y += other._collapse_button.size.y / 2.0
		
		draw_line(tree_line_start, tree_line_end, line_color)
		
		# draw lines going out to each child
		for child in get_children(false):
			# skip children marked as ignored
			if child.get_index(false) in _ignore_child_lines:
				continue
			# special case for if the child is also an accordion:
			# draw the line pointing to the header
			if child is AccordionMenu:
				var start := Vector2(line_start_x, child.position.y + child._collapse_button.size.y / 2)
				var end := start + Vector2(BASE_MARGIN / 2.0, 0.0)
				draw_line(start, end, line_color)
			else:
				var start := Vector2(line_start_x, child.position.y + child.size.y / 2)
				var end := start + Vector2(BASE_MARGIN / 2.0, 0.0)
				draw_line(start, end, line_color)
	
	if draw_background:
		var sb := get_theme_stylebox(&"background", &"AccordionMenu")
		var button_height := _collapse_button.get_rect().size.y
		var rect := Rect2(_collapse_button.position, size)
		rect.size.y -= button_height
		rect.position.y += button_height
		draw_style_box(sb, rect)


class CollapseButton extends Button:
	func _init(p_collapsed: bool = false, p_text: String = "") -> void:
		text = p_text
		icon = COLLAPSE_ICON_COLLAPSED if p_collapsed else COLLAPSE_ICON
		toggle_mode = true
		button_pressed = p_collapsed
		alignment = HORIZONTAL_ALIGNMENT_LEFT
		theme_type_variation = &"AccordionButton"
