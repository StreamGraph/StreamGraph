# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends ConfirmationDialog
class_name TwitchSetupDialog

func _ready():
	
	%Authenticate.pressed.connect(authenticate_twitch)
	%CopyURLButton.pressed.connect(copy_auth_link)
	#confirmed.connect(connect_to_chat)
	%Join_Chat.pressed.connect(connect_to_chat)

func authenticate_twitch(skip_load = false):
	
	#	Temporary setup for loading credentials
	var loaded_creds = null
	
	if !skip_load:
		loaded_creds = Connections.load_credentials("twitch") 
	
	if loaded_creds != null:
		
		Connections.twitch.token = loaded_creds.data.token
		if loaded_creds.data.has("channel"):
			
			%Default_Chat.text = loaded_creds.data.channel
			
		
		Connections.twitch.cache_user_data()
		# Binds invalid auth to call No_Twitch.authenticate_with_twitch, handles if the cache of auth is invalid. 
		Util.safe_connect(Connections.twitch.invalid_auth,authenticate_twitch.bind(true))
		
		connect_to_chat()
		return
		
	
#	Handles first authentication/invalid cached authentication.
	Util.safe_connect(Connections.twitch.token_received, save_twitch_token)
	
	var url = Connections.twitch.authenticate_with_twitch(%Client_ID.text)
	OS.shell_open(url)

func save_twitch_token(token):
	
	Connections.save_credentials({"token" : token}, "twitch")
	

func copy_auth_link():
	var url = Connections.twitch.authenticate_with_twitch(%Client_ID.text)
	DisplayServer.clipboard_set(url)


func connect_to_chat():
	
	if not Connections.twitch.get("chat_socket") or Connections.twitch.chat_socket.get_ready_state() != WebSocketPeer.STATE_OPEN:
		Connections.twitch.setup_chat_connection(%Default_Chat.text)
		return
	
	
	Connections.twitch.join_channel(%Default_Chat.text)
