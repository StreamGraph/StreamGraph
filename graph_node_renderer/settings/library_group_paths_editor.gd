# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends PanelContainer

@onready var paths_container: VBoxContainer = %PathsContainer
@onready var folder_dialog: FileDialog = %FolderDialog
@onready var add_button: Button = %AddButton

var _edited: FolderView


func _ready() -> void:
	for i in StreamGraphConfig.get_library_search_paths().size():
		var path: String = StreamGraphConfig.get_library_search_paths()[i]
		add_folder_view(path, i == 0)

	# calculate_move_buttons()
	folder_dialog.canceled.connect(_on_folder_dialog_canceled)
	
	add_button.pressed.connect(
		func():
			folder_dialog.dir_selected.connect(_on_folder_dialog_dir_selected_new, CONNECT_ONE_SHOT)
			folder_dialog.popup_centered()
	)


func calculate_move_buttons() -> void:
	await get_tree().process_frame
	for fv: FolderView in paths_container.get_children():
		fv.calculate_move_buttons()


func _on_folder_dialog_dir_selected_new(path: String) -> void:
	add_folder_view(path)
	StreamGraphConfig.add_library_search_path(path)


func add_folder_view(path: String, default: bool = false) -> void:
	var fv := FolderView.new(path, default)
	paths_container.add_child(fv)
	fv.open_dialog.connect(_on_folder_view_open_dialog.bind(fv))
	fv.deleted.connect(calculate_move_buttons)
	fv.moved.connect(calculate_move_buttons)
	calculate_move_buttons()


func _on_folder_view_open_dialog(path: String, who: FolderView) -> void:
	folder_dialog.current_dir = path
	folder_dialog.dir_selected.connect(_on_folder_dialog_dir_selected, CONNECT_ONE_SHOT)
	_edited = who
	folder_dialog.popup_centered()


func _on_folder_dialog_dir_selected(path: String) -> void:
	if not _edited:
		return
	
	StreamGraphConfig.rename_library_search_path(_edited.path, path)
	_edited.set_path(path)
	_edited = null


func _on_folder_dialog_canceled() -> void:
	Util.safe_disconnect(folder_dialog.dir_selected, _on_folder_dialog_dir_selected)
	Util.safe_disconnect(folder_dialog.dir_selected, _on_folder_dialog_dir_selected_new)
	_edited = null


class FolderView extends HBoxContainer:
	const REMOVE_ICON := preload("res://graph_node_renderer/textures/remove-icon.svg")
	const LOAD_ICON := preload("res://graph_node_renderer/textures/load-icon.svg")
	
	const ARROW_DOWN_ICON := preload("res://graph_node_renderer/textures/arrow-down-icon.svg")
	const ARROW_UP_ICON := preload("res://graph_node_renderer/textures/arrow-up-icon.svg")
	
	var label: Label
	var delete_button: Button
	var open_path_button: Button
	
	var move_down_button: Button
	var move_up_button: Button
	
	var path: String
	var is_default: bool = false
	
	signal open_dialog(path: String)
	signal deleted()
	signal moved()
	
	
	func _init(p_path: String = "", p_default: bool = false) -> void:
		path = p_path
		is_default = p_default

		label = Label.new()
		label.text = path.replace("$PWD", "app directory")
		label.size_flags_horizontal = Control.SIZE_EXPAND_FILL
		label.clip_text = true
		label.text_overrun_behavior = TextServer.OVERRUN_TRIM_ELLIPSIS
		label.tooltip_text = path
		
		delete_button = Button.new()
		delete_button.flat = true
		delete_button.icon = REMOVE_ICON
		delete_button.disabled = p_default
		delete_button.tooltip_text = "Delete"
		delete_button.pressed.connect(
			func():
				deleted.emit()
				StreamGraphConfig.remove_library_search_path(path)
				queue_free()
		)
		
		open_path_button = Button.new()
		open_path_button.flat = true
		open_path_button.icon = LOAD_ICON
		open_path_button.disabled = p_default
		open_path_button.tooltip_text = "Select path"
		open_path_button.pressed.connect(
			func():
				open_dialog.emit(path)
		)
		
		move_up_button = Button.new()
		move_up_button.flat = true
		move_up_button.icon = ARROW_UP_ICON
		move_up_button.tooltip_text = "Move up"
		move_up_button.pressed.connect(
			func():
				StreamGraphConfig.move_library_path_up(path)
				get_parent().move_child(self, get_index() - 1)
				moved.emit()
		)
		
		move_down_button = Button.new()
		move_down_button.flat = true
		move_down_button.icon = ARROW_DOWN_ICON
		move_down_button.tooltip_text = "Move down"
		move_down_button.pressed.connect(
			func():
				StreamGraphConfig.move_library_path_down(path)
				get_parent().move_child(self, get_index() + 1)
				moved.emit()
		)
		
		if p_default:
			label.tooltip_text += "\nThis is a default path. It cannot be changed or removed."
			delete_button.tooltip_text = "This is a default path. It cannot be changed or removed."
			move_up_button.tooltip_text = "This is a default path. It cannot be changed or removed."
			move_down_button.tooltip_text = "This is a default path. It cannot be changed or removed."
			open_path_button.tooltip_text = "This is a default path. It cannot be changed or removed."
			move_up_button.disabled = p_default
			move_down_button.disabled = p_default

		add_child(label)
		add_child(delete_button)
		add_child(open_path_button)
		add_child(move_down_button)
		add_child(move_up_button)
	
	
	func set_path(p_path: String) -> void:
		path = p_path
		label.text = path


	func calculate_move_buttons() -> void:
		if is_default:
			move_up_button.disabled = true
			move_down_button.disabled = true
			return
		
		move_up_button.disabled = get_index() == 1
		move_down_button.disabled = get_index() == get_parent().get_child_count() - 1
		
