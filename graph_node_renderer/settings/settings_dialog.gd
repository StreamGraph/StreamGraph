# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends AcceptDialog
class_name SettingsDialog

@onready var category_tree: Tree = %CategoryTree
@onready var category_content: Control = %CategoryContent
@onready var shortcuts_editor: ShortcutsEditor = %ShortcutsEditor


func _ready() -> void:
	var cr := category_tree.create_item()
	for i in category_content.get_children():
		var item := category_tree.create_item(cr)
		item.set_text(0, i.name)

	category_tree.item_selected.connect(
		func():
			var item := category_tree.get_selected()
			for i: Control in category_content.get_children():
				i.visible = i.get_index() == item.get_index()
	)
	
	canceled.connect(shortcuts_editor.ensure_no_focus)
	confirmed.connect(shortcuts_editor.ensure_no_focus)

	category_tree.set_selected(cr.get_child(0), 0)


func _unhandled_key_input(event: InputEvent) -> void:
	if not visible:
		return
	
	if event.keycode == KEY_ESCAPE and event.is_pressed():
		canceled.emit()
		hide()
