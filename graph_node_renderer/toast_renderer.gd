# (c) 2023-present Eroax
# (c) 2023-present Yagich
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
extends Control
class_name ToastRenderer


const ERROR_ICON = preload("res://graph_node_renderer/textures/error_icon_color.svg")
const WARNING_ICON = preload("res://graph_node_renderer/textures/warning_icon_color.svg")
const INFO_ICON = preload("res://graph_node_renderer/textures/info_icon.svg")

@export var hold_time: float = 1.5
@export var limit: int = 5

@onready var vbox: VBoxContainer = %VBoxContainer


func _ready() -> void:
	for i in DeckHolder.logger.toast_history.duplicate():
		_on_logger_toast(i.text, i.type)
	DeckHolder.logger.log_toast.connect(_on_logger_toast)


func _on_logger_toast(text: String, type: Logger.LogType) -> void:
	DeckHolder.logger.toast_history.remove_at(0)
	var panel := PanelContainer.new()
	panel.mouse_filter = Control.MOUSE_FILTER_IGNORE
	panel.theme_type_variation = &"ToastPanel"
	var hb := HBoxContainer.new()
	hb.mouse_filter = Control.MOUSE_FILTER_IGNORE
	var icon := TextureRect.new()
	icon.expand_mode = TextureRect.EXPAND_FIT_WIDTH_PROPORTIONAL
	icon.mouse_filter = Control.MOUSE_FILTER_IGNORE
	
	match type:
		Logger.LogType.INFO:
			icon.texture = INFO_ICON
		Logger.LogType.WARN:
			icon.texture = WARNING_ICON
		Logger.LogType.ERROR:
			icon.texture = ERROR_ICON
	
	var label := Label.new()
	label.text = text
	label.mouse_filter = Control.MOUSE_FILTER_IGNORE
	
	hb.add_child(icon)
	hb.add_child(label)
	
	panel.add_child(hb)
	
	move_and_remove()
	
	panel.modulate.a = 0.0
	vbox.add_child(panel)
	var t := panel.create_tween()
	t.tween_property(panel, ^"modulate:a", 1.0, 0.1)
	t.tween_interval(hold_time)
	t.tween_property(panel, ^"modulate:a", 0.0, 1.0)
	t.finished.connect(
		func():
			panel.queue_free()
	)


func move_and_remove() -> void:
	if not (vbox.get_child_count() + 1 > limit):
		return

	for i: Node in vbox.get_children().slice(0, limit):
		if i.get_meta(&"queued", false):
			continue
		
		i.set_meta(&"queued", true)
		var t := i.create_tween()
		t.tween_property(i, ^"position:x", i.position.x + i.size.x + 32, 0.4).set_trans(Tween.TRANS_BACK).set_ease(Tween.EASE_IN)
		t.finished.connect(
			func():
				i.queue_free()
		)
		break
	
